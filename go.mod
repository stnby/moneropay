module gitlab.com/moneropay/moneropay

go 1.16

require (
	github.com/gabstv/httpdigest v0.0.0-20200601123255-912d52c2d608 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/jackc/pgproto3/v2 v2.0.7 // indirect
	github.com/jackc/pgx/v4 v4.11.0 // indirect
	github.com/namsral/flag v1.7.4-pre // indirect
	gitlab.com/moneropay/go-monero v0.1.1 // indirect
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a // indirect
	golang.org/x/text v0.3.6 // indirect
)
